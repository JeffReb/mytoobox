MyTooBox Version Alpha 2
================================

Welcome to the MyTooBox SaaS-ERP - a fully-functional application that use modules to provide multiple solutions

What's inside
--------------

* Symfony Version 3.4 

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;
  
  * Doctrine ODM MongoDB (with alcaeus mongo-php-adapter for PHP7 Version);

  * Swiftmailer;

  * Annotations enabled for everything;
  
  * FOSUser Bundle to manage sessions and connections;
  
  * restClientBundle to access Full API REST
  
 * API REST Full
 
    * Front and Back End sources base to develop API FULL REST
    
 * SmartAdmin Framework fork from Bootstrap open source
 
    * Javascript sources that provide elements of graphical modules
    
    * Improved the access of your API REST
  


Installation requirements
-------------------------

The MyTooBox is configured to run with the following systems:

  * Apache2;

  * PHP >= 5.5.9 with this extensions:
    * curl
    * intl
    * mongodb driver (php 5: ext-mongo )(for PHP7 see ext-mongodb)
    
  * Mysql server
  
  * MongoDB server
  
  * NodeJS server with:
    * bower
    * gulp
    
  * Git


**Recommanded tools:**
  
     * Robo3t for mongodb access view
     * PHPMyAdmin or Adminer (best to minimum space) for mysql database access view
     * bower for nodeJS packaging
     * gulp for workflow
       


_Know Bugs:_

        Symfony vendor:
        A CHAQUE COMPOSER UPDATE OU INSTALL, -- FAIRE CORRECTION MANUEL SUR MONGODB ADAPTATEUR  
        (vendor/alcaeus/mongo-php-adapter/lib/Alcaeus/MongoDbAdapter/ExceptionConverter.php)
        SUIVANT INSTRUCTION CI-DESSOUS:     
        https://github.com/alcaeus/mongo-php-adapter/commit/b2b8c62209ebe051cddeb7b8fd3963b51a81a17e

_Links:_

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.4/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.4/doctrine.html
[8]:  https://symfony.com/doc/3.4/templating.html
[9]:  https://symfony.com/doc/3.4/security.html
[10]: https://symfony.com/doc/3.4/email.html
[11]: https://symfony.com/doc/3.4/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: https://symfony.com/doc/current/setup.html
